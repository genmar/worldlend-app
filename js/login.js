var app = {

	initialize: function() {

		Parse.$ = jQuery;
        Parse.initialize(CONFIG.APP_ID, CONFIG.JAVASCRIPT_KEY);
        Parse.serverURL = CONFIG.SERVER_URL;

        app.checkUser();

	},

	checkUser: function() {
		
		var currentUser = Parse.User.current();

        if (!currentUser) {
           app.setHandlers();
        } else {
        	window.location = '/lender/loan-requests.html';
        }

	},

	setHandlers: function() {

		$(document).on('click', '#plt-anchor-login', function (e) {
			e.preventDefault();
            utils.blockPage();
            app.signin();
            // TODO: Handle enter to signin
        });

	},

	signin: function() {

        var email = $('#plt-input-email').val();
        var password = $('#plt-input-password').val();

        if (email == null || email.length == 0 || password == null || password.length == 0) {
            utils.unblockPage();
        	alert('All fields are required');
            return;
        }

        if (!utils.validateEmail(email)) {
            utils.unblockPage();
        	alert('Invalid email address');
            return;
        }

        Parse.User.logIn(email, password, {
            success: function(user) {
                
                utils.unblockPage();

                window.location = '/lender/loan-requests.html';

            },
            error: function(user, error) {

                switch(error.code) {
                    case 205:
                        utils.unblockPage();
                        alert('We do not have an account with this email');
                        break;
                        // TODO: Handle more errors
                    default:
                        utils.showError(error);
                }

            }
        });

	},

    logout: function() {
        Parse.User.logOut().then(function(){
            window.localStorage.clear();
            utils.unblockPage();
            window.location = '/login.html';
        }, function(error){
            utils.showError(error);
        });
    }
	
}

app.initialize();