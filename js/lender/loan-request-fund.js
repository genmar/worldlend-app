var app = {

	setHandlers: function() {

		$('#plt-button-fund-loan').on('click', function(e) {
            e.preventDefault();
            app.fundLoan();
        });
		
	},

	fundLoan: function() {

		console.log('hi');

		utils.blockPage();

		var amountToFund = $('#plt-loan-amount').val();
		var loanObjectIdToBeFunded = utils.getUrlParameter('id');

		var data =	{
						amountToFund: amountToFund,
						loanObjectIdToBeFunded: loanObjectIdToBeFunded
					};

		Parse.Cloud.run('fundLoan', data, {
            success: function(response) {

            	utils.unblockPage();

            	console.log(response);

                if (response.message === 'fund_success') {
                    
                    window.location.href = response.httpResponse.data.data.redirect_url;

                } else {

                    console.log(response.message);

                }

            },
            error: function(error) {
                // window.location.reload();
                utils.logError(error);
            }
        });


	}
	
}

common.init();