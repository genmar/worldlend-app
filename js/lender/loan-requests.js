var app = {

	setHandlers: function() {
		

		app.getLoanRequests();

	},

	getLoanRequests: function() {

		utils.blockPage();

        var Loan = Parse.Object.extend('Loan');
        var query = new Parse.Query(Loan);
        query.equalTo('status','funding');
        query.descending('createdAt');
       
        query.find({
            success: function(loans) {

                if (loans.length > 0) {

                    app.renderLoans(loans);

                } else {
                    alert('No loans at the moment');
                    utils.unblockPage();
                }

            },
            error: function(error) {
                utils.showError(error);
            }
        });

	},

	renderLoans: function(loans) {

		for (var i = 0; i < loans.length; i++) {

			var loanObjectId = loans[i].id;

           	var loanAmount = loans[i].get('amount');
           	var loanAmountFunded = loans[i].get('amountFunded');

           	var loanTerm = loans[i].get('term');
           	var loanFactor = loans[i].get('factor');
           	var loanAim = loans[i].get('aim');

           	var loanAmountToPay = loanAmount * loanFactor;
           	var loanAmountRemaining = loanAmount - loanAmountFunded;

           	var percentageFunded = (loanAmountFunded / loanAmount) * 100;

           	var interestTotal = Math.round((loanFactor - 1) * 100);

            var loanCard = 	'<div class="col-xl-4 col-xxl-4">' +
								'<div class="row">' +
									'<div class="col-xl-12 col-lg-6 col-sm-6">' +
										'<div class="card overflow-hidden">' +
				                            '<div class="card-body">' +
				                                '<div class="text-center">' +
				                                    '<div class="text-center mb-4">' +
														'<h1 class="text-black mb-2 font-w600">$' + loanAmount + ' USD</h1>' +
														'<p class="mb-0 mt-2">' + loanTerm + ' months</p>' +
														'<h3 class="mt-4 mb-2 font-w500">' + loanAim + '</h3>' +
													'</div>' +
													'<a class="btn btn-outline-primary btn-rounded mt-3 px-5" href="/lender/loan-request-details.html?id=' + loanObjectId + '">Details</a>' +
													'<a class="btn btn-outline-primary btn-rounded mt-3 px-5" href="/lender/loan-request-fund.html?id=' + loanObjectId + '">Fund</a>' +
				                                '</div>' +
				                                '<div class="text-center mt-4 mb-4">' +
													'<span class="float-left">$0</span>' +
					                            	'<span class="float-right">$' + loanAmount + '</span><br>' +
					                                '<div class="progress" style="height:20px;">' +
					                                    '<div class="progress-bar bg-success" style="width: ' + percentageFunded + '%; height:20px;" role="progressbar"></div>' +
					                                '</div>' +
					                                '<span class="float-left text-sm mt-1">Funded: $' + loanAmountFunded + '</span>' +
												'</div>' +
											'</div>' +
											'<div class="card-footer pt-0 pb-0 text-center">' +
				                                '<div class="row">' +
													'<div class="col-4 pt-3 pb-3 border-right">' +
														'<h3 class="mb-1">' +
															'<img src="https://www.countryflags.io/mx/flat/32.png">' +
														'</h3>' +
														'<span>Country</span>' +
													'</div>' +
													'<div class="col-4 pt-3 pb-3 border-right">' +
														'<h3 class="mb-1">' +
															'<a href="javascript:void(0)" class="badge badge-rounded badge-primary">B4</a>' +
														'</h3>' +
														'<span>Score</span>' +
													'</div>' +
													'<div class="col-4 pt-3 pb-3">' +
														'<h3 class="mb-1">' + interestTotal + '%</h3><span>Interest</span>' +
				                                    '</div>' +
				                                '</div>' +
				                            '</div>' +
				                        '</div>' +
									'</div>' +
								'</div>' +
							'</div>';

            $('#plt-tbody-loans').append(loanCard);

        }

        utils.unblockPage();

	}
	
}

common.init();