var app = {

	initialize: function() {

		Parse.$ = jQuery;
        Parse.initialize(CONFIG.APP_ID, CONFIG.JAVASCRIPT_KEY);
        Parse.serverURL = CONFIG.SERVER_URL;

        app.checkUser();

	},

	checkUser: function() {
		
		var currentUser = Parse.User.current();

        if (!currentUser) {
           app.setHandlers();
        } else {
        	window.location = '/lender/loan-requests.html';
        }

	},

	setHandlers: function() {

		$(document).on('click', '#plt-anchor-signup', function (e) {
			e.preventDefault();
            utils.blockPage();
            app.signup();
            // TODO: Handle enter to signin
        });

	},

	signup: function() {

        var email = $('#plt-input-email').val();
        var password = $('#plt-input-password').val();
        
        if (email == null || email.length == 0) {
            utils.unblockPage();
            alert('All fields are required');
            return;
        }

        if (!utils.validateEmail(email)) {
            utils.unblockPage();
            alert('Invalid email address');
            return;
        }

        if (password == null || password.length == 0) {
            utils.unblockPage();
            alert('All fields are required');
            return;
        }

        var role = 'Borrower';
        role = (utils.getUrlParameter('type') === 'br') ? 'Borrower' : 'Lender';

        console.log(role);

        var user = new Parse.User();
        user.set('username',email);
        user.set('email',email);
        user.set('password',password);
        user.set('role',role);
        
        utils.blockPage();

        user.signUp(null, {
          success: function(user) {

            utils.unblockPage();

            if (role === 'Borrower') {
                window.location = '/borrower/onboarding.html';
            } else {
                window.location = '/lender/onboarding.html';
            }
            
            
          },
          error: function(user, error) {

            switch (error.code) {

                case 202:
                        utils.showToast('','Ya existe una cuenta con este correo electrónico','danger','icon-slash');
                        utils.unblockPage();
                        break;

                case 203:
                    utils.showToast('','Ya existe una cuenta con este correo electrónico','danger','icon-slash');
                    utils.unblockPage();
                    break;

                default: 
                    utils.showError(error);

            }

          }
        });

	},

    logout: function() {
        Parse.User.logOut().then(function(){
            window.localStorage.clear();
            utils.unblockPage();
            window.location = '/login.html';
        }, function(error){
            utils.showError(error);
        });
    }
	
}

app.initialize();