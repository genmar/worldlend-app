var common = {

    init: function() {

        Parse.$ = jQuery;
        Parse.initialize(CONFIG.APP_ID, CONFIG.JAVASCRIPT_KEY);
        Parse.serverURL = CONFIG.SERVER_URL;

        common.checkUser();

	},

    checkUser: function() {

        var currentUser = Parse.User.current();

        if (currentUser) {
            $('#plt-user-name').text(currentUser.get('name'));
            $('#plt-user-role').text(currentUser.get('role'));
        } else if (!currentUser) {  
            window.location.href = '/login.html';
        } else {
            common.logout();
        }

        common.setCommonHandlers();

    },

    handleParseError: function(error) {

        switch (error.code) {

            case Parse.Error.INVALID_SESSION_TOKEN:
                Parse.User.logOut();
                window.location = '/login.html';
                break;

            case 205:
                utils.unblockPage();
                alert('We do not have an account with this email');
                break;

            default: 
                utils.showError(error);

        }

    },

    logout: function() {
        Parse.User.logOut().then(function(){
            utils.unblockPage();
            window.location = "/login.html";
        }, function(error){
            common.handleParseError(error);
        });
    },

	setCommonHandlers: function() {

        $(document).on('click', '#plt-anchor-logout', function (e) {
            e.preventDefault();
            utils.blockPage();
            common.logout();
        });

        app.setHandlers();
	}

}