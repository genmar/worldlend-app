var app = {

	setHandlers: function() {

		$('.plt-pay-now').on('click', function(e) {
            e.preventDefault();
            var amount = $(this).data('amount');
            app.payLoan(amount);
        });
		
	},

	payLoan: function(amount) {

		console.log('hi');

		utils.blockPage();

		var amountToPay = amount;
		var loanObjectIdToBePaid = utils.getUrlParameter('URb5fgBGJs');

		var data =	{
						amountToPay: amountToPay,
						loanObjectIdToBePaid: loanObjectIdToBePaid
					};

		Parse.Cloud.run('payLoan', data, {
            success: function(response) {

            	utils.unblockPage();

            	console.log(response);

                if (response.message === 'fund_success') {
                    
                    window.location.href = response.httpResponse.data.data.redirect_url;

                } else {

                    console.log(response.message);

                }

            },
            error: function(error) {
                // window.location.reload();
                utils.logError(error);
            }
        });


	}
	
}

common.init();