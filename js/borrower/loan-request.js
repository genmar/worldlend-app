var app = {

	factors: {"3": 1.2, "6": 1.5, "12": 2.0},

	setHandlers: function() {

		$('#plt-loan-amount').on('input change',function(e){

			e.preventDefault();

			app.updateSimulation();

		});

		$('#plt-loan-term').on('input change',function(e){

			e.preventDefault();

			app.updateSimulation();

		});

		$('#plt-button-create-loan').on('click', function(e) {
            e.preventDefault();
            app.createLoan();
        });
	
		app.updateSimulation();

	},

	updateSimulation: function() {

		var currentAmount = parseFloat($('#plt-loan-amount').val());
		var currentTerm = parseFloat($('#plt-loan-term').val());

		var totalToPay = currentAmount * app.factors[currentTerm];
		var monthlyPayment = totalToPay / currentTerm;

		$('#ptl-loan-monthly-payment').text(utils.maskAmount(monthlyPayment))
		$('#ptl-loan-term-months').text(currentTerm);
		$('#plt-loan-total-to-pay').text(utils.maskAmount(totalToPay));

	},

	createLoan: function() {

		utils.blockPage();

		var currentAmount = parseFloat($('#plt-loan-amount').val());
		var currentTerm = parseFloat($('#plt-loan-term').val());

		var Loan = Parse.Object.extend('Loan');
		var loan = new Loan();
		
		loan.set('amount',currentAmount);
		loan.set('term',currentTerm);
		loan.set('factor',app.factors[currentTerm]);
		loan.set('owner',Parse.User.current());
		loan.set('status','funding');

		loan.save(null, {
		  success: function(newLoan) {

		  	utils.unblockPage();
		  	
		  	$('#plt-row-loan-request-header').hide();
			$('#plt-row-loan-request').hide();
			$('#plt-row-loan-request-typ').removeClass('d-none');
		  	

		  },
		  error: function(newLoanFail, error) {
		    utils.showError(error);
		  }
		});

	}
	
}

common.init();