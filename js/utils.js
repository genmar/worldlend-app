var utils = {

    showError: function(error) {
        utils.logError(error);
        utils.unblockPage();
        alert('Error. Try again please.');
    },

    logError: function(error) {
        console.log(error);
    },

    blockPage: function() {
        $.blockUI({ 
            message: '<h3>Loading...</h3>',
            css: { 
                border: 'none', 
                padding: '15px', 
                backgroundColor: '#000', 
                '-webkit-border-radius': '10px', 
                '-moz-border-radius': '10px', 
                opacity: .5, 
                color: '#fff' 
            } 
        });
    },

    unblockPage: function() {
        $.unblockUI();
    },

    validateEmail: function(email) {
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email);
    },

    getUrlParameter: function(sParam) {
        var sPageURL = decodeURIComponent(window.location.search.substring(1)),
            sURLVariables = sPageURL.split('&'),
            sParameterName,
            i;

        for (i = 0; i < sURLVariables.length; i++) {
            sParameterName = sURLVariables[i].split('=');

            if (sParameterName[0] === sParam) {
                return sParameterName[1] === undefined ? true : sParameterName[1];
            }
        }
    },

    roundAmount: function(amount) {
        var roundedAmount = Math.round(amount * 100) / 100;
        return roundedAmount;
    },

    maskAmount: function(amount) {

        // Mask amount using the JS Intl Api - https://hacks.mozilla.org/2014/12/introducing-the-javascript-internationalization-api/

        var formattedAmount = new Intl.NumberFormat("es-MX",
                            {   style: "currency", 
                                currency: "MXN",
                                minimumFractionDigits: 0
                            });

        return formattedAmount.format(Math.trunc(amount));

    },

    getFromLocalStorage: function(key) {
        return window.localStorage.getItem(key);
    },

    saveInLocalStorage: function(key,value){
        var storage = window.localStorage;
        storage.setItem(key,value);
    },

    getSizeCode: function(sizeLabel) {
        var sizeCode = sizeLabel.replace(/[()]/g,''); // remove parenthesis
        sizeCode = sizeCode.replace(/-/g,''); // remove dashes
        sizeCode = sizeCode.replace(/\s/g,''); // remove spaces
        sizeCode = sizeCode.replace(/\//g,''); // remove slashes
        return sizeCode.toString();
    },

    phoneFormat: function(s) {
        return s.toString().replace(/\d{2}(?=.)/g, '$& ');
    },

    phoneFormatForWa: function(s) {
        return s.toString().replace(/\s/g,'');
    }
 
}